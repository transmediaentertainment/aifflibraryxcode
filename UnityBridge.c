/*
 This is a bridge to the Aiff Library
 We are only implementing Open and Close File and Read Marker for our book authoring App
 More functions can be added easily if required
 */

#include <stdio.h>
#include "libaiff.h"

static AIFF_Ref _openedFile = NULL;
static float _sampleRate = 44100.0;
static int _markerID = 0;
static uint64_t _markerReadPosition;

const char* PrintHello(){
	return "Aiff Library Bridge Functioning 2";
}

int OpenFile( const char* fileName )
{
    // reset the read marker variables
    _markerID = 0;
    _markerReadPosition = 0;
    
    if ( _openedFile != NULL )
    {
        // a file is already opened
        // close it
        AIFF_CloseFile(_openedFile);
    }
    
    // Open the file
    _openedFile = AIFF_OpenFile(fileName, F_RDONLY);
    
    if ( _openedFile != NULL )
    {
        // we have a valid file open - get the sample rate
        
        float seconds;
        uint64_t nSamples ;
        int channels ;
        double samplingRate ;
        int bitsPerSample ;
        int segmentSize ;
        
        if( AIFF_GetAudioFormat( _openedFile, &nSamples, &channels, &samplingRate, &bitsPerSample, &segmentSize ) < 1 )
        {
            puts( "Error" ) ;
            AIFF_CloseFile( _openedFile ) ;
            _openedFile = NULL;
            return 0;
        }
        
        _sampleRate = (float)samplingRate;
        
        seconds = (float)nSamples / _sampleRate;
        
        /*
         * Print out the seconds
         */
        printf("File Opened Length: %f\n", seconds ) ;
        
        
    }
    
    return _openedFile != NULL;
}

int CloseFile(void)
{
    // reset the read marker variables
    _markerID = 0;
    _markerReadPosition = 0;
    
    int retval = AIFF_CloseFile(_openedFile);
    _openedFile = NULL;
    return retval;
}

float AIFF_ReadNextMarker()
{
    
    if ( _openedFile == NULL )
    {
        printf("No File Open\n");
        return -1.0;
    }
    
    float seconds;
    char* name;
    
    if ( AIFF_ReadMarker( _openedFile, &_markerID, &_markerReadPosition, &name ) < 1 )
    {
        // mo markers left
        printf("No More Markers\n");
        return -1.0;
    }
    
    seconds = (float)_markerReadPosition / _sampleRate;
    
    printf("Marker %d --> [%f]\n", _markerID, seconds );
    
    if( name )
    {
        printf( "(%s)\n", name ) ;
        free( name ) ;
    }
    
    return seconds;
}


